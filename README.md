# [repo.paradoxdev.com](repo.paradoxdev.com)

## Quick-Start

0. Install dependencies (listed as names of Arch-Linux packages):
  - `docker`
  - `docker-compose`

For example:

```bash
sudo pacman -S docker docker-compose
```

1. _Optional:_ Configure hostname in `/etc/hosts` (for testing):

```bash
echo '127.0.0.1 repo.paradoxdev.local' >> /etc/hosts
```

2. Configure credentials and settings:

```bash
cp .env.example .env
eval $EDITOR .env
```

3. Build/start containers:

```bash
docker compose up --build --remove-orphans # --detach
# To stop, use the following command:
docker compose down --remove-orphans
```

4. Wait for start-up to complete, then open the web-app (eg. `repo.paradoxdev.local`)

5. Obtain the GitLab instance's initial root password:

```bash
./scripts/get-initial-password.sh
```

6. Log-in to the web-app with the root credentials:
  - Username: `root`
  - Password: (printed in the previous step)

7. Obtain the runner registration token:
  - Open the context menu on the top left and click the "Admin" option
  - Navigate to `Overview -> Runners`
  - Click the "Register an instance runner" button
  - Copy the registration token

8. Register the runner service:
  - Run `./scripts/register-runner.sh`
  - Hit `<ENTER>` when given the prompt `Enter the GitLab instance URL` to accept the default
  - Paste the registration token from the previous step and hit `<ENTER>`
  - Hit `<ENTER>` to accept the defaults for all subsequent prompts

## Deploy

0. Ensure you have set up at least password-authenticated SSH access to the
  server (or remote machine) from your command node (local machine).
1. Create a user/group for the deployment on the server (eg.
  `repo-paradoxdev-deploy`):

_Remote Machine_:

```bash
mkdir -p /srv/paradoxdev

useradd --system \
  --user-group \
  --create-home \
  --home-dir /srv/paradoxdev/repo \
  --shell "`which bash`" \
  --comment 'repo.paradoxdev.com deployment' \
  --groups wheel,docker \
  repo-paradoxdev-deploy

passwd repo-paradoxdev-deploy
```

2. Set up SSH key access for your command node (eg. `10.10.10.1`):

_Local Machine_:

```bash
ssh-keygen -q \
  -t rsa \
  -b 4096 \
  -N '' \
  -f ~/.ssh/repo-paradoxdev-deploy

ssh-copy-id \
  -i ~/.ssh/repo-paradoxdev-deploy \
  repo-paradoxdev-deploy@10.10.10.1

# Test
ssh -i ~/.ssh/repo-paradoxdev-deploy repo-paradoxdev-deploy@10.10.10.1 exit 0

cat ~/.ssh/repo-paradoxdev-deploy \
  | xclip -selection clipboard
```



3. Configure SSH:

_Local Machine_ (`~/.ssh/config`)

```sshconfig
Host repo-paradoxdev-deploy
    Hostname 10.10.10.1
    User repo-paradoxdev-deploy
    IdentityFile ~/.ssh/repo-paradoxdev-deploy
    RequestTTY auto
```

_Remote Machine_ (`/etc/ssh/sshd_config`):

```sshdconfig
# Comment the following line out:
#PasswordAuthentication no
```

_Remote Machine_:

```bash
systemctl restart sshd
```

4. In your `gitlab.com` repository, set the following CI/CD file variables:
  - `ENV_FILE`: contents of `.env.example` with proper credentials/information
  - `DEPLOY_ENV`: credentials/information gathered from previous steps (example
    below)
  - `STAGE_ENV`: similar to `DEPLOY_ENV` but with credentials/information
    replaced by reproducing the previous steps with a different username (eg.
    `repo-paradoxdev-stage`)

_`DEPLOY_ENV` (or `STAGE_ENV`)_:

```bash
REPO_WORKDIR=/srv/paradoxdev/repo

GITLAB_PORT_HTTP=8001
GITLAB_PORT_HTTPS=4431
GITLAB_PORT_SSH=2201

DDNS_HOSTNAME=repo.paradox.com
DDNS_USERNAME=abcdefghijklmnopqrstuvwxyz
DDNS_PASSWORD=zyxwvutsrqponmlkjihgfedcba

# Server SSH credentials/information for deployment (only needed for CI/CD)
SSH_ADDR=${DDNS_HOSTNAME:?}
SSH_PORT=2222
SSH_USER=repo-paradoxdev-deploy
SSH_PRIV_KEY="\
-----BEGIN OPENSSH PRIVATE KEY-----
cpgOP+W3FoonFoJTOCc7Is4jUVh6WgZ9q9PDWPrYhijdmnHSOYwtwmT7qmJXVELcMNrbpD
b4TPBVcAZixxXs2xj4flWgWzCBAcALmnLNHa+XQQ9ChV2jBVd4bhJQsx4lyURNObvVVMX4
...
H7xsJ7mUu85NdgVrFVGqj/WhMwP42andwyrDO+4/vPKWhZXVI0xoEGZ6wPoReJ5z98eIjg
INgi0tGebXErJ8Ixl7QspRzwj+JBf1vOyXoPJQOu46+CQPLf1Qtom5hVGpRI5/VosYqYAH
-----END OPENSSH PRIVATE KEY-----
"
```

## Environment

The environment should be configured depending on the use-case:

- `.env`: local test options or common deployment options
  (example: [`.env.example`](./.env.example))
- `.stage.env`: info, credentials, and `.env` overrides for deploying to staging
  (example: [`.stage.env.example`](./.stage.env.example))
- `.deploy.env`: info, credentials, and `.env` overrides for deploying to production
  (example: [`.deploy.env.example`](./.deploy.env.example))

### Docker

__General__

- `GITLAB_PORT_HTTP`: the port to use for HTTPS (for letsencrypt)
  (default: `8080`)
- `GITLAB_PORT_HTTPS`: the port to use for HTTPS
  (default: `8443`)
- `GITLAB_PORT_SSH`: the port to use for SSH
  (default: `8022`)
- `REPO_WORKDIR`: base directory for bind mounts
  (default: `./out`; *note:* relative paths are not acceptable for deployment[^1])
- `GITLAB_HOSTNAME`: hostname for the GitLab instance
  (default: `repo.paradoxdev.com`)
- `GITLAB_RUNNERS`: number of runner instances to deploy
  (default: `1`)
- `DDCLIENT_CONFIG`: full contents of the 'ddclient' configuration file
  (default: see [`.deploy.env.example`](./.deploy.env.example))

__CI/CD__

- `CI_REGISTRY_IMAGE`: base URI of the container registry
  (default: `paradoxdev`)

[^1]: https://docs.docker.com/compose/compose-file/#volumes

### CI

- `SSH_ADDR`: the URL to use to SSH into the host server
- `SSH_PORT`: the port to use to SSH into the host server
- `SSH_USER`: the user to use to SSH into the host server
- `SSH_PRIV_KEY`: the contents of the private key to use to SSH into the host server

## Troubleshooting

__SSH password authentication error `Permission denied, please try again`__

Disable PAM in `/etc/sshd_config`:

```sshdconfig
# Comment out the following line:
#UsePAM yes
```

__Firefox throws `SEC_ERROR_REUSED_USSUER_AND_SERIAL` while testing__

```bash
cd ~/.mozilla/firefox
# Replace all 'cert_override.txt' files
find . -name 'cert_override.txt' -or -name 'cert9.db' -exec mv '{}' '{}.old' \;
```

__Password not working__

See: <https://docs.gitlab.com/ee/security/reset_user_password.html#use-a-rails-console>

1. Open the Rails console:

```bash
docker compose -p repoparadoxdevcom -it gitlab gitlab-rails console
```

2. Reset the password:

```ruby
user = User.find(1) # user ID of the root user
pass = ::User.random_password
user.password = pass
user.password_confirmation = pass
user.save!
exit
```
