server_host = `hostname`.strip
external_url "https://#{server_host}"

letsencrypt['enable'] = true
letsencrypt['contact_emails'] = ['nhein@paradoxdev.com']

gitlab_rails['signup_enabled'] = false
gitlab_rails['initial_root_password'] = File.read('/etc/default/gitlab/initial_root_password.txt').gsub("\n", "")
