#!/usr/bin/env bash

set -e

SRCDIR=`realpath -e $(dirname "${BASH_SOURCE[0]}")/..`
cd "${SRCDIR}"

source .env

docker compose exec gitlab \
	grep 'Password:' /etc/gitlab/initial_root_password

# vim:ft=bash
