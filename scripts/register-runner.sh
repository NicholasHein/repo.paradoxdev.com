#!/usr/bin/env bash

set -e
readonly INDEX=${1:?index of the runner is required}

SRCDIR=`realpath -e $(dirname "${BASH_SOURCE[0]}")/..`
cd "${SRCDIR}"

source .env

docker compose exec -it --index ${INDEX} runner \
	gitlab-runner register --url "https://${GITLAB_HOSTNAME}:${GITLAB_PORT_HTTPS:-8443}"

# vim:ft=bash
